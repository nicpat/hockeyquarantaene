let requireesm = require("esm")(module);
const { routes } = requireesm("./src/router/index.js");
const { messages } = requireesm("./src/plugins/i18n.js");

module.exports = {
  transpileDependencies: ["vuetify"],
  pwa: {
    workboxOptions: {
      skipWaiting: true
    }
  },
  pluginOptions: {
    sitemap: {
      baseURL: "https://hockeyquarantaene.ch",
      hashMode: true,
      defaults: {
        lastmod: new Date(),
        changefreq: "daily",
        priority: 0.5
      },
      pretty: process.env.NODE_ENV !== "production",
      routes
    }
  },
  chainWebpack: config => {
    config.plugin("html").tap(args => {
      args[0]["meta_vars"] = {
        description: messages.de.app.meta.description,
        title: messages.de.overview.meta.title,
        image: messages.de.app.meta.image,
        url: messages.de.app.url
      };
      return args;
    });
  }
};
