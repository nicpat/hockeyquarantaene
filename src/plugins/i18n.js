import Vue from "vue";
import VueI18n from "vue-i18n";

export const messages = {
  de: {
    app: {
      name: "hockeyquarantaene.ch",
      url: "https://hockeyquarantaene.ch",
      languages: {
        label: "Sprache",
        de: "Deutsch",
        en: "English"
      },
      meta: {
        titleTemplate: "%s | National League Quarantäne-Liste",
        title: "{title} | National League Quarantäne-Liste",
        description:
          "Eine Liste mit allen Teams der National League, die momentan in Quarantäne sind oder bereits in der Quarantäne waren.",
        image: "https://hockeyquarantaene.ch/table.png"
      }
    },
    overview: {
      title:
        "Zurzeit ist kein Team in der Quarantäne.|Zurzeit ist ein National-League-Team in Quarantäne:|Zurzeit sind {n} National-League-Teams in Quarantäne:",
      table: "National League Quarantäne-Liste",
      share: {
        title:
          "Alle Quarantänefälle in der National League|Zurzeit ist ein National-League-Team in Quarantäne|Zurzeit sind {n} National-League-Teams in Quarantäne"
      },
      meta: {
        title: "Übersicht"
      }
    },
    team: {
      quarantined:
        "Die {team} sind seit {date} in Quarantäne.|Der {team} ist seit {date} in Quarantäne.",
      duration: "Die Quarantäne dauert noch {duration}.",
      notquarantined:
        "Die {team} sind zurzeit nicht in Quarantäne.|Der {team} ist zurzeit nicht in Quarantäne.",
      pastquarantines:
        "Die {team} waren bis jetzt {number} mal in der Quarantäne|Der {team} war bis jetzt {number} mal in der Quarantäne",
      share: {
        notquarantined:
          "Alle Quarantänefälle der {team}|Alle Quarantänefälle des {team}",
        quarantined:
          "Die {team} sind bis am {date} in Quarantäne|Der {team} ist bis am {date} in Quarantäne"
      }
    },
    quarantine: {
      team: "Team",
      from: "Von",
      until: "Bis",
      sources: "Quellen",
      since: "Seit {date}",
      remaining: "Noch {duration}",
      details: "Details"
    },
    footer: {
      foph: {
        title: "Bitte halte dich an die Hygiene- und Verhaltensregeln des BAG",
        button: "Informationen des BAG",
        link: "https://bag-coronavirus.ch/",
        text:
          "Damit leistest du einen wichtigen Beitrag dazu, dass wir bald wieder im Stadion Eishockey sehen dürfen und diese Webseite überflüssig wird!"
      },
      twitter: {
        title: "Tweets von hkyquarantaene"
      },
      sihf: {
        title:
          "Den aktuellen Spielplan mit allen Spielverschiebungen findest du hier",
        link:
          "https://www.sihf.ch/de/game-center/national-league#/today/league/asc/page/0/",
        button: "Spielplan SIHF.ch"
      },
      contact: {
        title:
          "Hast du einen Fehler gefunden oder möchtest sonstwie Kontakt aufnehmen",
        button: "Kontakt via Twitter"
      },
      contribution: {
        title: "Oder trage selber zu diesem Projekt bei",
        button: "Code auf Gitlab"
      }
    }
  },
  en: {
    app: {
      name: "hockeyquarantaene.ch",
      url: "https://hockeyquarantaene.ch",
      languages: {
        label: "Language",
        de: "Deutsch",
        en: "English"
      },
      meta: {
        titleTemplate: "%s | National League Quarantine List",
        title: "{title} | National League Quarantine List",
        description:
          "A list of all National League teams that are currently quarantined or have been quarantined in the past.",
        image: "https://hockeyquarantaene.ch/table.png"
      }
    },
    overview: {
      title:
        "Currently, there is no team quarantined.|Currently, there is one National League team quarantined:|Currently, there are {n} National League teams quarantined:",
      table: "National League Quarantine List",
      share: {
        title:
          "All quarantine cases of the National League|Currently, there is one National League team quarantined|Currently, there are {n} National League teams quarantined"
      },
      meta: {
        title: "Overview"
      }
    },
    team: {
      quarantined:
        "The {team} are quarantined since {date}.|{team} ist quarantined since {date}.",
      duration: "They will remain in quarantine for {duration}.",
      notquarantined:
        "The {team} are currently not quarantined.|{team} is currently not quarantined.",
      pastquarantines:
        "The {team} have been quarantined {number} times|{team} has been quarantined {number} times",
      share: {
        notquarantined:
          "All quarantine cases of the {team}|All quarantine cases of {team}",
        quarantined:
          "The {team} are quarantined until {date}|{team} is quarantined until {date}"
      }
    },
    quarantine: {
      team: "Team",
      from: "From",
      until: "Until",
      sources: "Sources",
      since: "Since {date}",
      remaining: "{duration} remaining",
      details: "Details"
    },
    footer: {
      foph: {
        title: "Please respect the hygiene rules of the FOPH",
        button: "Information of the FOPH",
        link: "https://foph-coronavirus.ch/",
        text:
          "Your contribution to these rules is important! It plays a big part making ice hockey games with spectators possible in the future again."
      },
      twitter: {
        title: "Tweets by hkyquarantaene"
      },
      sihf: {
        title: "You can find the current schedule with all postponements here",
        link:
          "https://www.sihf.ch/de/game-center/national-league#/today/league/asc/page/0/",
        button: "Schedule SIHF.ch"
      },
      contact: {
        title: "Have you found an error or want to contact me",
        button: "PM me on Twitter"
      },
      contribution: {
        title: "Or contribute to this project",
        button: "Code on Gitlab"
      }
    }
  }
};

Vue.use(VueI18n);

export default new VueI18n({
  locale: "en",
  fallbackLocale: "en",
  messages
});
