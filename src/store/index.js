import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    teams: null
  },
  mutations: {
    setTeams(state, payload) {
      state.teams = payload;
    }
  },
  getters: {
    getTeam: state => id => {
      return state.teams[id];
    },
    getAllQuarantines: state => (teamAbbreviation = null) => {
      let quarantines = [];

      if (!state.teams) {
        return null;
      }

      Object.values(
        teamAbbreviation
          ? { 0: state.teams[teamAbbreviation.toLowerCase()] }
          : state.teams
      ).forEach(team => {
        team.quarantines.forEach(quarantine => {
          let entry = quarantine;
          entry.team = team.details;

          quarantines.push(entry);
        });
      });

      return quarantines.sort(function(a, b) {
        return new Date(b.from) - new Date(a.from);
      });
    },
    getActiveQuarantines: (state, getters) => (team = null) => {
      let activeQuarantines = [];

      let allQuarantines = getters.getAllQuarantines(team);
      if (!allQuarantines) {
        return null;
      }
      allQuarantines.forEach(quarantine => {
        let startDate = new Date(quarantine.from);
        let endDate = new Date(
          startDate.getTime() + 1000 * 60 * 60 * 24 * (quarantine.length + 1)
        );
        let now = new Date();

        if (startDate <= now && now <= endDate) {
          activeQuarantines.push(quarantine);
        }
      });

      return activeQuarantines;
    }
  },
  actions: {
    loadTeams({ state, commit }) {
      return new Promise(resolve => {
        if (state.teams === null) {
          this._vm.$axios.get("/data/teams.json").then(response => {
            commit("setTeams", response.data);
            resolve(state.teams);
          });
        } else {
          resolve(state.teams);
        }
      });
    }
  },
  modules: {}
});
