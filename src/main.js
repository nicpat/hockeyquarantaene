import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import i18n from "./plugins/i18n";
import VueMeta from "vue-meta";
import VueGtag from "vue-gtag";

Vue.config.productionTip = false;

// Moment JS
const moment = require("moment");
//require("moment/locale/de-ch");

Vue.use(require("vue-moment"), {
  moment
});

// Axios
Vue.prototype.$axios = require("axios");

// Vue Meta
Vue.use(VueMeta, {
  keyName: "head"
});

// Vue Gtag
Vue.use(
  VueGtag,
  {
    config: {
      id:
        process.env.NODE_ENV === "production" ? "G-METYCB5BH7" : "G-X74F23JMEZ"
    }
  },
  router
);

new Vue({
  router,
  store,
  i18n,
  vuetify,
  render: h => h(App)
}).$mount("#app");
