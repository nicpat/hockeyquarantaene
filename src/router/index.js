let teams = require("../../public/data/teams.json");

import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

export const routes = [
  {
    path: "/",
    name: "Overview",
    component: () =>
      import(/* webpackChunkName: "overview" */ "../views/Overview.vue"),
    meta: {
      sitemap: {
        priority: 1
      }
    }
  },
  {
    path: "/:team",
    name: "Team",
    component: () => import(/* webpackChunkName: "team" */ "../views/Team.vue"),
    meta: {
      sitemap: {
        slugs: Object.keys(teams)
      }
    }
  }
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes
});

export default router;
